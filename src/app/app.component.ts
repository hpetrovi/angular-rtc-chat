import { Component, ChangeDetectorRef, OnInit } from '@angular/core';

import * as wswarm from 'webrtc-swarm';
import * as signalhub from 'signalhub';
import * as split from 'split2';
import * as through from 'through2';
import * as onend from 'end-of-stream';
import * as randombytes from 'randombytes';

enum MessageType {
  INTRO = 1,
  MESSAGE
}

enum AppType {
  REACT = 1,
  ANGULAR
}

interface Stream {
  type: MessageType;
  value: {
    nick?: string;
    message?: string;
  };
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  swarm;
  streams: { [id: string]: any };
  seen: { [id: string]: boolean };
  users: {};
  usersClone: {};
  title;
  appTypes;
  messages: any[];
  currentUserNick: string;
  chatInput: string;
  nickModel: string;

  introMessage = {
    type: MessageType.INTRO,
    value: {
      nick: '',
      app: AppType.ANGULAR
    }
  };

  constructor(private _changeDetectorRef: ChangeDetectorRef) {
    this.streams = {};
    this.chatInput = '';
    this.currentUserNick = '';
    this.appTypes = {
      angular: 1,
      react: 0
    };
    this.seen = {};
    this.users = {};
    this.usersClone = {};
    this.messages = [];
    this.nickModel = '';
  }

  // Choose nick
  chooseNick(nick: string) {
    this.initializeConnection(nick, 'react-vs-angular');
  }

  // Chat
  handleIncomingStream = (stream, id) => {
    // console.log('CONNECTED', id);
    this.streams[id] = stream;
    onend(stream, this.onStreamEnd(id));

    stream.write(JSON.stringify(this.introMessage) + '\n');

    stream.pipe(split()).pipe(through(this.handleIncomingMessage(id)));
  }

  handleIncomingMessage = (id) => {
    return (line, enc, next) => {
      const msg = line.toString();
      const msgid = msg.type === MessageType.MESSAGE ? msg.value.id : randombytes(8).toString('hex');
      if (this.addMsg(msgid, JSON.parse(msg), id) === false) {
        return next();
      }
      // Object.keys(this.streams).forEach(sid => {
      //   if (sid === id) {
      //     return;
      //   }
      //   this.streams[id].write(line + '\n');
      // });
      next();
    };
  }

  onStreamEnd = (id) => {
    return () => {
      if (this.usersClone[id].app === AppType.ANGULAR) {
        this.appTypes.angular--;
      } else if (this.usersClone[id].app === AppType.REACT) {
        this.appTypes.react--;
      }
      this.seen[this.usersClone[id].introMessageId] = false;
      delete this.streams[id];
      delete this.users[id];

      this._changeDetectorRef.detectChanges();
    };
  }

  addMsg = (messageId, message, streamId) => {
    if (this.seen[messageId]) {
      return false;
    }
    if (message.type === MessageType.INTRO) {
      this.users = {
        ...this.users,
        [streamId]: {
          introMessageId: messageId,
          app: message.value.app,
          nick: message.value.nick
        }
      };
      this.usersClone = {
        ...this.usersClone,
        [streamId]: {
          introMessageId: messageId,
          app: message.value.app,
          nick: message.value.nick
        }
      };

      if (message.value.app === AppType.ANGULAR) {
        this.appTypes.angular++;
      } else if (message.value.app === AppType.REACT) {
        this.appTypes.react++;
      }
    } else if (message.type === MessageType.MESSAGE) {
      this.messages.push({
        streamId,
        message: message.value.message
      });
    }

    // console.log(this.users);

    this.seen[messageId] = true;


    this._changeDetectorRef.detectChanges();
  }

  getAppTypeAsClass(appType: number) {
    if (appType === AppType.ANGULAR) {
      return { 'chat__user-list__list__item__status-icon--angular': true };
    } else if (appType === AppType.REACT) {
      return { 'chat__user-list__list__item__status-icon--react': true };
    }
  }

  getNickByAppTypeClass(appType: number, me: boolean): {} {
    return {
      'chat__messages__message__nick--angular': appType === AppType.ANGULAR && !me,
      'chat__messages__message__nick--react': appType === AppType.REACT && !me,
      'chat__messages__message__nick--react--me': me
    };
  }

  getObjectKeys(object): string[] {
    return Object.keys(object);
  }

  chat = (ev) => {
    const messageId = randombytes(8).toString('hex');
    const message = {
      type: MessageType.MESSAGE,
      value: {
        id: messageId,
        message: ev.target.value,
        sender: {
          ...this.users['CURRENT_USER']
        }
      }
    };
    Object.keys(this.streams).forEach((id) => {
      this.streams[id].write(JSON.stringify(message) + '\n');
    });
    this.addMsg(messageId, message, 'CURRENT_USER');
    this.chatInput = '';
  }

  initializeConnection(nick: string, channelName: string) {
    this.users = {
      'CURRENT_USER': {
        nick,
        app: this.introMessage.type
      }
    };
    this.usersClone = {
      'CURRENT_USER': {
        nick,
        app: this.introMessage.type
      }
    };

    this.introMessage.value.nick = nick;

    this.currentUserNick = nick;
    this.swarm = wswarm(
      signalhub(channelName, ['https://signalhub-hzbibrznqa.now.sh'])
    );

    this.swarm.on('peer', this.handleIncomingStream);
  }

  // Init
  ngOnInit() {
  }
}
