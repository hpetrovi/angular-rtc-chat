import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'usersFilter'})
export class UsersFilter implements PipeTransform {
  transform(users: any[]): any[] {
    return users.filter(user => {
      return user !== 'CURRENT_USER';
    });
  }
}
